const startButton = document.getElementById('start-btn')
const backButton = document.getElementById('back-btn')
const nextButton = document.getElementById('next-btn')
const ctaButton = document.getElementById('cta-btn')
const quizIntro = document.getElementById('quizIntro')
const quizQuestion = document.getElementById('quizQuestion')
const quizInput = document.getElementById('quizInput')
const quizButtons = document.getElementById('quizButtons')
const apneaResultsDisplay = document.getElementById('quizResultsApnea')
const insomniaResultsDisplay = document.getElementById('quizResultsInsomnia')
const paraResultsDisplay = document.getElementById('quizResultsPara')
const legResultsDisplay = document.getElementById('quizResultsLeg')
const hyperResultsDisplay = document.getElementById('quizResultsHyper')
const cleanResultsDisplay = document.getElementById('quizResultsClean')

let quizQuestions, currentQuestionIndex
let apneaScore = 0
let epWorthScore = 0
let insomniaScore = 0
let legScore = 0
let parasomniaScore = 0
let hypersomniaScore = 0
let feet = 0
let inches = 0
let weight = 0
let age = 0
let bodyMassIndex = 0

startButton.addEventListener('click', startQuiz)
    /* This is the nextButton trigger event, cycling through the list of questions and choosing the next one. */
nextButton.addEventListener('click', () => {
        currentQuestionIndex++
        if (quizQuestions.length > currentQuestionIndex) {
            displayQuestion()
        } else {
            apneaResultsDisplay.classList.remove('hide')
            insomniaResultsDisplay.classList.remove('hide')
            paraResultsDisplay.classList.remove('hide')
            apneaResultsDisplay.classList.remove('hide')
            legResultsDisplay.classList.remove('hide')
            hyperResultsDisplay.classList.remove('hide')
            cleanResultsDisplay.classList.remove('hide')
            displayResults()
        }
    })
    /* This is the backButton trigger event, cycling through the list of questions and choosing the previous one. */
backButton.addEventListener('click', () => {
    currentQuestionIndex--
    displayQuestion()
})

function displayResults() {
    calculateBodyMassIndex(feet, inches, weight)
    resetState()
    results = quizResults
    quizQuestion.innerHTML = "Here are your results: \n"
    backButton.classList.add('hide')
    nextButton.classList.add('hide')
    if (epWorthScore >= 10 || (age >= 50 && bodyMassIndex >= 30) || apneaScore >= 3) {
        apneaResultsDisplay.innerHTML = quizResults[0].result
        showCallToAction()
    }
    if (insomniaScore >= 2) {
        insomniaResultsDisplay.innerHTML = quizResults[1].result
        showCallToAction()
    }
    if (legScore >= 2) {
        legResultsDisplay.innerHTML = quizResults[2].result
        showCallToAction()
    }
    if (parasomniaScore == 2) {
        paraResultsDisplay.innerHTML = quizResults[3].result
        showCallToAction()
    }
    if (hypersomniaScore == 1) {
        hyperResultsDisplay.innerHTML = quizResults[4].result
        showCallToAction()
    }
    if (epWorthScore < 10 && (age < 50 && bodyMassIndex < 30) && apneaScore < 3 && insomniaScore < 2 && legScore < 2 && parasomniaScore < 2 && hypersomniaScore < 1) {
        quizQuestion.classList.add('hide')
        cleanResultsDisplay.innerHTML = "Based on your answers this brief quiz did not indicate risk for some of the common sleep disorders. Feel free to consult us if you would like a more in depth conversation with a specialist."
        hideCallToAction()
    }
}

function hideCallToAction() {
    ctaButton.classList.add('hide')
}

function showCallToAction() {
    ctaButton.classList.remove('hide')
}

function calculateBodyMassIndex(feet, inches, weight) {
    var feetToInches = feet * 12
    var totalInches = parseInt(inches) + parseInt(feetToInches)
    var totalInchesSquared = parseInt(totalInches) * parseInt(totalInches)
    var calculatedBodyMassIndex = (parseInt(weight) / parseInt(totalInchesSquared)) * 703
    bodyMassIndex = parseInt(calculatedBodyMassIndex)
}

function startQuiz() {
    startButton.classList.add('hide')
    nextButton.classList.remove('hide')
    quizQuestion.classList.remove('hide')
    quizIntro.classList.add('hide')
    currentQuestionIndex = 0
    displayQuestion()
}

function displayQuestion() {
    quizQuestions = questions
    resetState()
    showNextQuestion(quizQuestions[currentQuestionIndex])
    if (currentQuestionIndex === 0) {
        backButton.classList.add('hide')
    }
    if (currentQuestionIndex > 0) {
        backButton.classList.remove('hide')
    }
}

function showNextQuestion(question) {
    if (currentQuestionIndex >= 7 && currentQuestionIndex < 14) {
        quizQuestion.innerText = "On a scale of 0-3, rate your chance of dozing off in the following situations. \n \n" + question.question
    } else {
        quizQuestion.innerText = question.question
    }
    question.answers.forEach(answer => {
        if (answer.input === false) {
            const button = document.createElement("button")
            button.classList.add('btn')
            button.innerText = answer.text
            button.setAttribute('value', answer.score)
            quizButtons.classList.remove('hide')
            quizButtons.appendChild(button)
            button.addEventListener('click', selectAnswer)
        }
        if (answer.input === true) {
            const input = document.createElement("input")
            input.classList.add('input-field')
            quizInput.classList.remove('hide')
            input.setAttribute('placeholder', answer.text)
            input.setAttribute('id', answer.id)
            input.setAttribute('max', answer.max)
            input.setAttribute('type', answer.type)
            input.setAttribute('min', answer.min)
            quizInput.appendChild(input)
            input.addEventListener('input', verifyInput)
        }
    })
}

function verifyInput(e) {
    var input = e.target
    var inputValue = input.value
    var inputId = input.id
    nextButton.classList.remove('hide')
    nextButton.addEventListener('click', addInputs(inputId, inputValue))
}

function addInputs(id, value) {

    if (id == 'age') {
        age = value
    }
    if (id == 'weight') {
        weight = value
    }
    if (id == 'inches') {
        inches = value
    }
    if (id == 'feet') {
        feet = value
    }

}

function selectAnswer(e) {
    const selectedButton = e.target
    selectedButton.classList.add('selected')
    const score = selectedButton.getAttribute('value')
    nextButton.classList.remove('hide')
    nextButton.addEventListener('click', addScore(score))
}

function addScore(score) {
    if (currentQuestionIndex <= 7) {
        if (score == 1) {
            apneaScore = apneaScore + 1
        } else {
            apneaScore = apneaScore
        }
    }
    if (currentQuestionIndex >= 7 && currentQuestionIndex < 14) {
        if (score == 1) {
            epWorthScore = epWorthScore + 1
        }
        if (score == 2) {
            epWorthScore = epWorthScore + 2
        }
        if (score == 3) {
            epWorthScore = epWorthScore + 3
        } else {
            epWorthScore = epWorthScore
        }
    }
    if (currentQuestionIndex >= 14 && currentQuestionIndex < 17) {
        if (score == 1) {
            insomniaScore = insomniaScore + 1
        } else {
            insomniaScore = insomniaScore
        }
    }
    if (currentQuestionIndex >= 17 && currentQuestionIndex < 20) {
        if (score == 1) {
            legScore = legScore + 1
        } else {
            legScore = legScore
        }

    }
    if (currentQuestionIndex >= 20 && currentQuestionIndex < 22) {
        if (score == 1) {
            parasomniaScore = parasomniaScore + 1
        } else {
            parasomniaScore = parasomniaScore
        }
    }
    if (currentQuestionIndex == 22) {
        if (score == 1) {
            hypersomniaScore = hypersomniaScore + 1
            console.log(hypersomniaScore)
        } else {
            hypersomniaScore = hypersomniaScore
        }
    }
}

function resetState() {
    nextButton.classList.add('hide')
    while (quizInput.firstChild) {
        quizInput.removeChild(
            quizInput.firstChild
        )
    }
    while (quizButtons.firstChild) {
        quizButtons.removeChild(
            quizButtons.firstChild
        )
    }

}

const inputArray = [{}]

const w = "- no chance of dozing"
const x = "- mild chance of dozing"
const y = "- moderate chance of dozing"
const z = "- high chance of dozing"

const questions = [{
        question: "What is your gender?",
        answers: [{
                text: "Male",
                input: false,
                score: 1
            },
            {
                text: "Female",
                input: false,
                score: 0
            }
        ]
    },
    {
        question: "What is your age?",
        answers: [{
            text: "Input your age",
            id: "age",
            input: true,
            type: 'number',
            max: 120
        }]
    },
    {
        question: "What is your height? (in feet and inches)",
        answers: [{
                text: "Feet",
                id: "feet",
                input: true,
                type: 'number',
                min: 0,
                max: 10
            },
            {
                text: "Inches",
                id: "inches",
                input: true,
                type: 'number',
                min: 0,
                max: 11
            }
        ]
    },
    {
        question: "What is your weight? (in pounds)",
        answers: [{
            text: "Input your weight",
            id: "weight",
            input: true,
            type: 'number',
            min: 0,
            max: 999
        }]
    },
    {
        question: "Do you snore loudly?",
        answers: [{
                text: 'Yes',
                score: 1,
                input: false
            },
            {
                text: 'No',
                score: 0,
                input: false
            }
        ]
    },
    {
        question: "Have you ever been told you stop breathing at night?",
        answers: [{
                text: 'Yes',
                score: 1,
                input: false
            },
            {
                text: 'No',
                score: 0,
                input: false
            }
        ]
    },
    {
        question: "Do you have hypertension?",
        answers: [{
                text: 'Yes',
                score: 1,
                input: false
            },
            {
                text: 'No',
                score: 0,
                input: false
            }
        ]
    },
    {
        question: "Sitting and reading",
        answers: [{

                text: '0 ' + w,
                score: 1,
                input: false
            },
            {
                text: '1 ' + x,
                score: 1,
                input: false
            },
            {
                text: '2 ' + y,
                score: 2,
                input: false
            },
            {
                text: '3 ' + z,
                score: 3,
                input: false
            }
        ]
    },
    {
        question: "Sitting inactive in a public place",
        answers: [{

                text: '0 ' + w,
                score: 1,
                input: false
            },
            {
                text: '1 ' + x,
                score: 1,
                input: false
            },
            {
                text: '2 ' + y,
                score: 2,
                input: false
            },
            {
                text: '3 ' + z,
                score: 3,
                input: false
            }
        ]
    },
    {
        question: "Sitting quietly after lunch without alcohol",
        answers: [{

                text: '0 ' + w,
                score: 1,
                input: false
            },
            {
                text: '1 ' + x,
                score: 1,
                input: false
            },
            {
                text: '2 ' + y,
                score: 2,
                input: false
            },
            {
                text: '3 ' + z,
                score: 3,
                input: false
            }
        ]
    },
    {
        question: "Stopped for a few minutes in traffic",
        answers: [{

                text: '0 ' + w,
                score: 1,
                input: false
            },
            {
                text: '1 ' + x,
                score: 1,
                input: false
            },
            {
                text: '2 ' + y,
                score: 2,
                input: false
            },
            {
                text: '3 ' + z,
                score: 3,
                input: false
            }
        ]
    },
    {
        question: "As a car passenger for an hour",
        answers: [{

                text: '0 ' + w,
                score: 1,
                input: false
            },
            {
                text: '1 ' + x,
                score: 1,
                input: false
            },
            {
                text: '2 ' + y,
                score: 2,
                input: false
            },
            {
                text: '3 ' + z,
                score: 3,
                input: false
            }
        ]
    },
    {
        question: "Sitting and talking to someone",
        answers: [{

                text: '0 ' + w,
                score: 1,
                input: false
            },
            {
                text: '1 ' + x,
                score: 1,
                input: false
            },
            {
                text: '2 ' + y,
                score: 2,
                input: false
            },
            {
                text: '3 ' + z,
                score: 3,
                input: false
            }
        ]
    },
    {
        question: "Lying down to rest in the afternoon if circumstances permit",
        answers: [{

                text: '0 ' + w,
                score: 1,
                input: false
            },
            {
                text: '1 ' + x,
                score: 1,
                input: false
            },
            {
                text: '2 ' + y,
                score: 2,
                input: false
            },
            {
                text: '3 ' + z,
                score: 3,
                input: false
            }
        ]
    },
    {
        question: "Do you have difficulty falling asleep at least three nights a week for the last 3 months or longer?",
        answers: [{
                text: 'Yes',
                score: 1,
                input: false
            },
            {
                text: 'No',
                score: 0,
                input: false
            }
        ]
    },
    {
        question: "Do you have frequent unwanted awakenings in the middle of the night for the last 3 months or longer?",
        answers: [{
                text: 'Yes',
                score: 1,
                input: false
            },
            {
                text: 'No',
                score: 0,
                input: false
            }
        ]
    },
    {
        question: "Do you feel that your inability to sleep well is affecting the quality of your life and ability to function normally during the day?",
        answers: [{
                text: 'Yes',
                score: 1,
                input: false
            },
            {
                text: 'No',
                score: 0,
                input: false
            }
        ]
    },
    {
        question: "Do you have an uncomfortable urge to move your legs?",
        answers: [{
                text: 'Yes',
                score: 1,
                input: false
            },
            {
                text: 'No',
                score: 0,
                input: false
            }
        ]
    },
    {
        question: "Do the uncomfortable leg symptoms occur or worsen before beditme or during periods of inactivity like sitting in a car or plane?",
        answers: [{
                text: 'Yes',
                score: 1,
                input: false
            },
            {
                text: 'No',
                score: 0,
                input: false
            }
        ]
    },
    {
        question: "Do the symptoms improve when you walk or move around?",
        answers: [{
                text: 'Yes',
                score: 1,
                input: false
            },
            {
                text: 'No',
                score: 0,
                input: false
            }
        ]
    },
    {
        question: "Do you have abnormal movements or behaviors at night such as falling out of bed, sleep, walking, sleep eating or other unusual behaviors?",
        answers: [{
                text: 'Yes',
                score: 1,
                input: false
            },
            {
                text: 'No',
                score: 0,
                input: false
            }
        ]
    },
    {
        question: "Have you ever unknowingly injured yourself or others while sleeping?",
        answers: [{
                text: 'Yes',
                score: 1,
                input: false
            },
            {
                text: 'No',
                score: 0,
                input: false
            }
        ]
    },
    {
        question: "Do you have severe daytime sleepiness that affects your ability to function well during the day despite getting at least 7-8 hours of regular sleep every night?",
        answers: [{
                text: 'Yes',
                score: 1,
                input: false
            },
            {
                text: 'No',
                score: 0,
                input: false
            }
        ]
    },
]

const quizResults = [{
        result: "Your answers indicate you may be a risk for sleep apnea."
    },
    {
        result: "Your answers indicate you may have insomnia."
    },
    {
        result: "Your answers indicate you may have restless leg syndrome."
    },
    {
        result: "Your answers indicate you may have a parasomnia."
    },
    {
        result: "Your answers indicate you may have hypersomnia that needs further evaulation."
    }
]