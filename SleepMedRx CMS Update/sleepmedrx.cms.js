const loginButton = document.getElementById('login-button')
const authField = document.getElementById('auth-input')
let apiInput = authField.value
const Webflow = require('webflow-api')
const webflow = new Webflow({ token: apiInput })

loginButton.addEventListener('onclick', returnValue())

function returnValue() {
    webflow.info()
        .then((info => console.log(info)))
}